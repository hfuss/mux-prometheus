package main

import (
	"net/http"

	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	muxprom "gitlab.com/hfuss/mux-prometheus/pkg/middleware"
)

func main() {
	r := mux.NewRouter()

	instrumentation := muxprom.NewDefaultInstrumentation()
	r.Use(instrumentation.Middleware)

	r.Path("/metrics").Handler(promhttp.Handler())
	r.Path("/helloworld/{id}").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {})

	srv := &http.Server{Addr: "localhost:8080", Handler: r}
	srv.ListenAndServe()
}
